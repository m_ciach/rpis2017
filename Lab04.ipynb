{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Monte Carlo Estimation\n",
    "\n",
    "**Homework:** Problem **4d**, due December 23 2017, 10:59 AM."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Problem 4a (Monte Carlo volume estimation).** Monte Carlo is a method of computing integrals by simulating some random variables (this is called *stochastic simulations*). In this problem our goal is to estimate the area of a unit disk. We will do this by simulating random variables form the square. The probability that a point belongs to the disk is equal to the ratio of disk volume to the square's volume. Therefore, by multiplying this estimated probability by the area of the square, we obtain an estimate of the disk's volume. \n",
    "\n",
    "Implement in Python a function that for a given $N$:\n",
    " * Uniformly samples $N$ points in a $[-M,M]\\times[-M,M]$ square for a given $M$.\n",
    " * For each sampled point checks whether or not it lies inside a unit disk. \n",
    " * Returns the ratio $AX/N$, where $A$ is the area of the square, and $X$ is the number of points inside the disk.\n",
    "\n",
    "Then:\n",
    " * Run your function $1000$ times for $N=1000$ and $M=1$. Draw a histogram of the estimates.\n",
    " * What is the expected value $E(AX/N)$? What is the standard deviation?\n",
    " * Repeat the experiment, but this time sample from $[-10,10]\\times[-10,10]$. Again draw a histogram, and compute the moments. Discuss the results.\n",
    " \n",
    "*Hints:*\n",
    "\n",
    "* Remember to use vectorized arithmetic!\n",
    "\n",
    "* Functions like `sum` automatically cast boolean variables to integers. Therefore, for a boolean vector `B`, the function `sum(B)` will return the number of True coordinates.\n",
    "\n",
    "**Remark:** Since we know how to compute the area of a unit disk, this exercise seems rather pointless. However, note that the exact same procedure can be used to estimate the $n$-dimensional volume of any body $S$ provided that:\n",
    " * We have a black-box that tells us whether or not a given point is in $S$.\n",
    " * We know $M$ such that $S \\subseteq [-M,M]^n$ (or more generally we know a set $T$ such that we can sample uniformly from $T$ and know the volume of $T$).\n",
    " * The ratio of the volume of $S$ to the volume of $[-M,M]^n$ (or $T$) is not too small."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Problem 4b (Monte Carlo integration).** In this problem our goal is to compute the value of a definite integral using random sampling. Let $f:[-1,1]\\rightarrow\\mathbb{R}$ be defined as $f(x) = 2\\sqrt{1-x^2}$. We will proceed as follows. Let $X\\sim Unif([-1, 1])$ be a uniform random variable. The density of this variable is equal to $0.5$ over it's domain. From the definition of expected value for continuous variables, which was introduced in the last lecture, we have \n",
    "\n",
    "$$ \\mathbb{E}f(X) = \\int_{-1}^{1}f(x) \\frac{1}{2} dx.$$\n",
    "\n",
    "It follows that we can compute the integral by estimating $2\\mathbb{E}f(X)$.\n",
    "\n",
    "Implement a Python function that for a given $N$:\n",
    " * Uniformly samples $N$ points $x_1,\\ldots,x_N$ in $[-1,1]$.\n",
    " * Computes an estimate of $\\int_{-1}^1 f(x)dx$ using $f(x_1),\\ldots,f(x_N)$. As a part of this task, you need to figure out how to use the samples to compute the integral. \n",
    "\n",
    "What is the expected value, variance and the standard deviation of your method? You don't need to compute exact values, it is sufficient to give some simple formulas which depend on $f$.\n",
    "\n",
    "What is the connection between this task and the previous one? In other words, what function did we integrate in Problem 4a? Was it somehow connected to this one? Which method gave lower variance?\n",
    "\n",
    "**Remark:** Again, the specific problem we are solving is really simple. But our method works for any function, in particular for functions $f:\\mathbb{R}^n \\rightarrow \\mathbb{R}$. When $n$ is large, the standard quadrature-based integration breaks, but Monte Carlo approach in general still works (depending on $f$ additional variance-reducing tricks might be necessary, we will see one in the next problem). Further reading, for those interested: https://en.wikipedia.org/wiki/Curse_of_dimensionality"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Problem 4c (Monte Carlo counting).** Consider the following problem: Given a DNF formula $\\phi$, compute the number of assignments satisfying $\\phi$. Doing this is #P hard, so efficient exact solutions are unlikely. But we can try finding a good estimate. Your task in this problem is to use an approach similar to the one in **Problem 4a** to find such an estimate. \n",
    "\n",
    "A DNF formula looks like this:\n",
    "$$ \\phi = (x_1 \\wedge \\neg x_3 \\wedge x_4) \\vee (\\neg x_1 \\wedge x_2)$$\n",
    "This particular formula can be encoded using Python lists as follows:\n",
    "$$ phi = [ [1,-3,4],[-1,2]]$$\n",
    "\n",
    "Write a Python function which estimates the number of satisfying assignments for a given DNF formula $\\phi$ for $M$ variables using Monte Carlo sampling. Next, using a histogram (or other methods) decide whether this is an efficient method. First, check your results on the $\\phi$ formula above. Next, check the results for a random formula with $M=40$ variables. Use $100$ runs per $100$ samples. \n",
    " \n",
    "*Hints:*\n",
    "\n",
    "* An assignment can be simulated by drawing random vectors of -1/1 variables. The length of the vector is $M$.\n",
    "\n",
    "* To estimate the number of satisfying assignments, first compute the proportion of satisfying assignments in all simulated assignments. Next, mutiply this proportion by the number of all possible assignments.\n",
    "\n",
    "* To check whether an 'and' clause is satisfied (i.e. one list in $\\phi$), first take the variables which are present in this clause. Next, multiply the assignment and the clause coordinate-wise and check the signs of the coordinates of the resulting vector.\n",
    "\n",
    "* Note that it is enough that one 'and' clause is satisfied. Therefore, when you arrive at a satisfied clause, you don't need to check the rest.\n",
    "\n",
    "Below is a formula for simulating random DNF formulas for $M$ variables with $L$ 'and' clauses. The lengths of the 'and' clauses are drawn from the Poisson distribution. The expected number of variables in a clause is $M/2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[1, 3, -4], [4, 2, 1, 3]]"
      ]
     },
     "execution_count": 45,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def generate_DNF(M, L):\n",
    "    phi = []\n",
    "    for i in range(L):\n",
    "        l = min(rd.poisson(lam=M/2), M-1) + 1  # at least one and at most M variables in a clause\n",
    "        clause = np.random.choice(range(1,M+1),l,replace=False)\n",
    "        true_false = 2*np.random.randint(2,size=l)-1  # drawing -1/1 variables \n",
    "        clause *= true_false\n",
    "        phi.append(list(clause))\n",
    "    return(phi)\n",
    "    \n",
    "phi = generate_DNF(4, 2)\n",
    "phi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Problem 4d (Monte Carlo counting ctd).** The problem with the approach in **4c** is that the number of satisfying assignments is extremely small compared to the number of all possible assignments. To overcome this problem, we will use the technique of *importance sampling*. Namely, we will use a different distribution on assignments - we will only sample the *important* ones. \n",
    "\n",
    "The general idea is a bit similar to the rejection method, in the sense that we use an auxilliary distribution instead of the desired one. However, instead of rejecting, we multiply the result by a special weight. \n",
    "\n",
    "Formally, suppose we want to compute $\\mathbb{E}h(X)$ for some random variable $X: \\Omega \\rightarrow E$ with density $f(x)$. Recall that by definition,\n",
    "\n",
    "$$\\mathbb{E}h(X) =  \\int_{E} h(x) f(x) dx.$$\n",
    "\n",
    "Now, suppose that we have a random variable $Y$ with density $g(y)$, which is in some sense better than $X$. We want to estimate $\\mathbb{E}h(X)$ by sampling variables $Y$. Of course, it's not enought to estimate $\\mathbb{E}h(Y)$, because that has nothing to do with $\\mathbb{E}X$. However, if we estimate a slightly different expression, namely $\\mathbb{E}\\left( h(Y)\\frac{f(Y)}{g(Y)} \\right)$, then look what happens:\n",
    "\n",
    "$$\\mathbb{E}\\left( h(Y)\\frac{f(Y)}{g(Y)} \\right) = \\int_{E} h(x)\\frac{f(x)}{g(x)} g(x) dx = \\int_{E} h(x)f(x) dx = \\mathbb{E}h(X).$$\n",
    "\n",
    "We get exactly what we want - the expected value of $h(X)$ - by sampling from $Y$ and weighting the sampled values. Now, how to estimate $\\mathbb{E}h(Y)\\frac{f(Y)}{g(Y)}$? That's actually really straightforward - just sample $y_1, y_2, \\dots, y_N$, compute $h(y_i)$, multiply it by the weight $\\frac{f(y_i)}{g(y_i)}$ and take the mean. Formally, \n",
    "\n",
    "$$\\frac{1}{N} \\sum_{i=1}^N h(y_i) \\frac{f(y_i)}{g(y_i)} \\approx \\mathbb{E} \\left( h(Y)\\frac{f(Y)}{g(Y)} \\right) = \\mathbb{E}X. $$\n",
    "\n",
    "In practice, the only thing that's different to what we've already done is that we sample from a different distribution and weight the results before taking the mean.\n",
    "\n",
    "In our case, $X$ is a random (uniform) assignment to variables, and $Y$ is a random assignment which *satisfies* a DNF formula. It's better than $X$, because for large formulas the probability of satisfying an assignment is very low. Because of that, almost all of the $X$ assignments will not satisfy the formula, and our results won't be credible - they will have a huge variance.\n",
    "\n",
    "To sample $Y$, consider the following sampling scheme $\\mathcal{S}$ for assignments:\n",
    " * Choose a clause $C_i$ in $\\phi$ uniformly at random.\n",
    " * Set the values of the variables in $C_i$ so that $C_i$ is satisfied. \n",
    " * Set the values of the remaining variables randomly.\n",
    "\n",
    "This scheme samples only satisfying assignments. Moreover, it can sample any satisfying assignment. Our auxilliary variable, $Y$, is the result of this assignment. \n",
    "\n",
    "**Your tasks:**\n",
    "\n",
    " * For a given assignment $\\pi$ what is the probability of sampling $\\pi$ in the scheme $\\mathcal{S}$? In other words, what is the value of $g(\\pi) = \\mathbb{P}(Y = \\pi)$?\n",
    " * Find a function $h(x)$ which will be suitable for this task. After you have found the function, try to simplify the formula for the estimated number of satistying assignments as much as possible. \n",
    " * Implement a function which samples form scheme $\\mathcal{S}$ and estimates the number of assignments satisfying a DNF formula $\\phi$ using the technique of importance sampling. The function should accept the number of samples, the number of variables, and the formula. Vectorized solutions are encouraged, but not required this time.\n",
    " * Compare the sample mean and variance of the results for $\\phi$ from **4c** using e.g. a histogram.\n",
    " \n",
    " \n",
    "*Hints:* \n",
    "\n",
    "* The resulting formula for estimation of the number of satisfying assignments is very simple. The whole function should have at most approximately 15 lines of code.\n",
    "\n",
    "* You can use `np.sign()` for a vectorized *signum* function"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
